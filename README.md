# CasAgency Scan

Le Scanner de Réseau sur Mesure

CasAgency Scan est bien plus qu'un simple outil de scan de réseau. C'est une solution sur mesure conçue pour les professionnels de la sécurité informatique et les experts en pentest. Avec une interface utilisateur conviviale et des fonctionnalités puissantes, CasAgency Scan offre des capacités avancées pour explorer, analyser et sécuriser les réseaux avec une précision exceptionnelle.

Caractéristiques Principales :
------------------------------

Modes de Scan Polyvalents :

[Scan Complet :] Obtenez une vue exhaustive de tous les services, ports et vulnérabilités possibles.

[Scan Furtif :] Explorez discrètement les réseaux sans laisser de traces.

[Scan Avancé :] Personnalisez votre approche avec des options détaillées pour un contrôle total.

Scripting de Vulnérabilités :
----------------------
Intégrez les puissants scripts de vulnérabilités de Nmap pour identifier les failles de sécurité.
Importez et utilisez vos propres scripts personnalisés pour des analyses spécifiques.

Proxychains Intégré :
----------------------
Utilisez des proxies avec Proxychains pour diversifier vos sources et éviter la détection.

Analyse des Firewalls :
----------------------
Surmontez les obstacles avec un mode dédié pour analyser les configurations des firewalls.
Personnalisation Avancée :

Choisissez parmi une variété de modes IP, définissez des ports spécifiques, ajustez l'intervalle entre les requêtes.

Interface Intuitive :
---------------------
Une interface graphique conviviale qui rend la navigation et la configuration simples.

Résultats et Analyse :
----------------------
Visualisez rapidement les résultats avec des détails IP par IP. Analysez les vulnérabilités détectées avec une fonctionnalité intégrée.

Sauvegarde Facile :
----------------------
Enregistrez vos résultats dans différents formats, du texte brut aux fichiers Metasploit.

CasAgency Scan offre une flexibilité totale, des performances professionnelles et une sécurité avancée pour répondre aux exigences les plus strictes des professionnels de la sécurité informatique. Avec son architecture sur mesure, il s'adapte à vos besoins spécifiques et vous donne le contrôle total sur vos analyses de réseau. Découvrez une nouvelle ère de scans de réseau avec CasAgency Sca


Lancement du logiciel
----------------------

```
# Installer les dépendances
apt-get update
apt-get install -y nmap python3-pip python3-pyqt5

# Installer les dépendances Python
pip3 install PyQt5

```
```
cd /casagency-scan/
chmod +x casagency-scan.py

```
![dépot kaisen modif](scan-cas.png)




Installation du .deb
--------------------
```
dpkg -i casagency-scan.deb
```

![dépot kaisen modif](dpkg-install.png)


